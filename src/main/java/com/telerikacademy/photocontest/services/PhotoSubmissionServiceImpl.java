package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.DuplicateEntityException;
import com.telerikacademy.photocontest.exceptions.UnauthorizedOperationException;
import com.telerikacademy.photocontest.models.PhotoSubmission;
import com.telerikacademy.photocontest.models.Score;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import com.telerikacademy.photocontest.repositories.contracts.ScoreRepository;
import com.telerikacademy.photocontest.services.contracts.PhotoSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class PhotoSubmissionServiceImpl implements PhotoSubmissionService {
    private final PhotoSubmissionRepository photoSubmissionRepository;
    private final ScoreRepository scoreRepository;

    @Autowired
    public PhotoSubmissionServiceImpl(PhotoSubmissionRepository photoSubmissionRepository, ScoreRepository scoreRepository) {
        this.photoSubmissionRepository = photoSubmissionRepository;
        this.scoreRepository = scoreRepository;
    }

    @Override
    public void create(PhotoSubmission photoSubmission) {
        List<PhotoSubmission> existing_check=photoSubmissionRepository.getByContestIdandUser(photoSubmission.getUser().getUser_id(),
                photoSubmission.getContest().getContest_id());
        if(existing_check.size()!=0){
            throw new DuplicateEntityException("User", "submission", existing_check.get(0).getTitle());
        }
        photoSubmissionRepository.create(photoSubmission);
    }

    @Override
    public PhotoSubmission getByID(int id) {
        return photoSubmissionRepository.getById(id);
    }

    @Override
    public List<PhotoSubmission> getPhotoSubmissionsByContestId(int id) {
        return photoSubmissionRepository.getAllByContestID(id);
    }

    @Override
    public String delete(int id, User loggeduser) {
        if (!photoSubmissionRepository.getById(id).getUser().getUsername().equals(loggeduser.getUsername()))
            throw new UnauthorizedOperationException("You are not the author of this item or an admin");
        return photoSubmissionRepository.delete(id);
    }

    @Override
    public List<PhotoSubmission> searchByTitle(Optional<String> title) {
        return photoSubmissionRepository.searchByTitle(title);
    }

    @Override
    public PhotoSubmission setScore(int photoSubmissionId, Score score) {
        PhotoSubmission photoSubmission = photoSubmissionRepository.getById(photoSubmissionId);
        Set<Score> scores = photoSubmission.getScores();
        scores.add(score);
        scoreRepository.create(score);
        photoSubmissionRepository.update(photoSubmission);
        return photoSubmission;
    }
//    This is for cases where contest is finished
    @Override
    public PhotoSubmission setTotalScore(int photoSubmissionId) {
        PhotoSubmission photoSubmission = photoSubmissionRepository.getById(photoSubmissionId);
        photoSubmission.setTotalScore(scoreRepository.getTotalByPhotoId(photoSubmissionId));
        photoSubmissionRepository.update(photoSubmission);
        return photoSubmission;
    }
    @Override
    public PhotoSubmission getByContestId(int id, int contestId){
        List<PhotoSubmission> photoSubmissions = photoSubmissionRepository.getByUserID(id);
        PhotoSubmission photoSubmission = photoSubmissions
                .stream()
                .filter(p -> p.getContest().getContest_id() == contestId).findAny().orElse(null);
        return photoSubmission;
    }

    @Override
    public void deleteByContestID(int id){
        photoSubmissionRepository.DeleteByContestID(id);
    }

}
