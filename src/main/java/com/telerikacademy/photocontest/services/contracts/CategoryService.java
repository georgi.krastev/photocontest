package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.RoleType;

import java.util.List;

public interface CategoryService {
    List<CategoryType> getAll();
    CategoryType getByID(int id);
}
