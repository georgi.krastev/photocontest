package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List <User> getAll(Optional<String> search, User initiator);
    User getById(int id, User initiator);
    User getByUsername(String username);
    User create (User user);
    User updateSelf(User user);
    User updateById(User initiator, User newUserInfo);
    String delete(int id, User initiator);
    long getNumberOfUsers();
    List<User> getActiveWithRoleIdAndBadgeAtLeast(int roleId, int minIdValue, User initiator);
    List<User> getActiveWithRoleIdAtLeast(int minRoleId, User initiator);
    void updateResetPasswordToken(String token, String email);
    User getByResetPasswordToken(String token);
    void updatePassword(User user, String newPassword);
}
