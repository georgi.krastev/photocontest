package com.telerikacademy.photocontest.services.contracts;

import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.RoleType;

import java.util.List;

public interface PhaseTypeService {
    public PhaseType getPhaseTypeByID(int id);
    public List<PhaseType> getPhaseTypes();
}
