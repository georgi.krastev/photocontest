package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.repositories.contracts.ImageRepository;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    private ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public Image getImageByUserId(int user_id) {
        try {
            return imageRepository.getImageByUserId(user_id);
        }
        catch(ObjectNotFoundException e){
            throw new ObjectNotFoundException(e.getMessage());
        }
    }

    @Override
    public Image createImage(Image image) {
        return imageRepository.createImage(image);
    }

    @Override
    public Image censorImage(Image image) {
        return imageRepository.update(image);
    }

    @Override
    public Image getImageById(int id) {
        return imageRepository.getImageById(id);
    }

    @Override
    public String deleteImage(int id) {
        return imageRepository.deleteImage(id);
    }

    @Override
    public List<Image> getAdultImages() {
        return imageRepository.getAdultImages();
    }

}
