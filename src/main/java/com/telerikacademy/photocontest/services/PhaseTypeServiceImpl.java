package com.telerikacademy.photocontest.services;

import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.repositories.contracts.PhaseTypeRepository;
import com.telerikacademy.photocontest.services.contracts.PhaseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseTypeServiceImpl implements PhaseTypeService {

    private final PhaseTypeRepository phaseTypeRepository;
    @Autowired
    public PhaseTypeServiceImpl(PhaseTypeRepository phaseTypeRepository) {
        this.phaseTypeRepository = phaseTypeRepository;
    }

    @Override
    public PhaseType getPhaseTypeByID(int id){
        return phaseTypeRepository.getById(id);
    }

    @Override
    public List<PhaseType> getPhaseTypes(){
        return phaseTypeRepository.getAll();
    }

}
