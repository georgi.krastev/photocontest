package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.models.RoleType;
import com.telerikacademy.photocontest.repositories.contracts.RoleTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RoleTypeRepositoryImpl extends AbstractReadRepository<RoleType> implements RoleTypeRepository {
    public RoleTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(RoleType.class, sessionFactory);
    }
}
