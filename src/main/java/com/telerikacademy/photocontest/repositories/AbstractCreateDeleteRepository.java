package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.repositories.contracts.BaseCreateDeleteRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractCreateDeleteRepository<T> extends AbstractReadRepository <T>
        implements BaseCreateDeleteRepository<T> {

    public AbstractCreateDeleteRepository(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }
    public T create(T entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
            return entity;
        }
    }

    public String delete(int id) {
        T toDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(toDelete);
            session.getTransaction().commit();
            return String.format("%s with %d has been deleted", clazz.getSimpleName(), id);
        }
    }

    @Override
    public String delete(T entity) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
            return String.format("%s has been deleted", clazz.getSimpleName());
        }
    }
}
