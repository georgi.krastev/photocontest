package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.PhaseType;

import java.util.List;

public interface PhaseTypeRepository extends BaseCRUDRepository<PhaseType> {

//    List<PhaseType> getPhaseTypes();

}
