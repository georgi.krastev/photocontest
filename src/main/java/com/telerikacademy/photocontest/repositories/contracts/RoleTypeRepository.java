package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.RoleType;

public interface RoleTypeRepository extends BaseReadRepository<RoleType> {

}
