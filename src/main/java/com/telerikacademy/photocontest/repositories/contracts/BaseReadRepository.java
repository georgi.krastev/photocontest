package com.telerikacademy.photocontest.repositories.contracts;

import java.util.List;

public interface BaseReadRepository<T> {
    List<T> getAll();

    T getById(int id);

    <V> T getUniqueByField(String name, V value);

    public <V> List<T> getListByField(String name, V value);



}
