package com.telerikacademy.photocontest.repositories.contracts;

import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.ContestPhase;
import com.telerikacademy.photocontest.models.PhaseType;

import java.util.List;

public interface ContestPhaseRepository extends BaseCRUDRepository<ContestPhase> {

    List<Contest> getContestsByPhase(int phaseTypeId);

    List<Contest> getAllByPhase(int phase_type_id);
    List<Integer> getAllToBeMarkedAsFinished();
    List<ContestPhase> getAllPhasesByContestID(int id);

    ContestPhase getPhaseByPhaseNumberAndContestID(int phaseId, int contestId);

    List<PhaseType> getPhaseTypes();

    Integer getContestPhase(int contestID);
}
