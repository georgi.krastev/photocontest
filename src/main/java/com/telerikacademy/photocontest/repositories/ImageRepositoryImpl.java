package com.telerikacademy.photocontest.repositories;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.Contest;
import com.telerikacademy.photocontest.models.Image;

import com.telerikacademy.photocontest.repositories.contracts.ImageRepository;
import com.telerikacademy.photocontest.repositories.contracts.PhotoSubmissionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

//TODO add unsupporteed operation on the methods that are not implemented
@Repository
public class ImageRepositoryImpl extends AbstractCRUDRepository<Image> implements ImageRepository {

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        super(Image.class, sessionFactory);
    }

    @Override
    public Image getImageById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Image image = session.get(Image.class, id);
            if (image == null) {
                throw new EntityNotFoundException("Image");
            }
            return image;
        }
    }

    @Override
    public Image getImageByUserId(int user_id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery("SELECT  u.photo.id from User u where user_id = :id");
            query.setParameter("id", user_id);
            if(query.list().size()==0){
                throw new ObjectNotFoundException("Image","user_id",String.valueOf(user_id));
            }
            Integer image_id = query.list().get(0).getImage_id();
            if(image_id==null){
                throw new ObjectNotFoundException("Image","user_id",String.valueOf(user_id));
            }
            Query<Image> query2 = session.createQuery("from Image where image_id = :id");
            query2.setParameter("id", image_id);
            if(query2.list().size()==0){
                throw new ObjectNotFoundException("Image","user_id",String.valueOf(user_id));
            }
            Image image = query2.list().get(0);

            return image;
        }
    }

    @Override
    public Image createImage(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.save(image);
        }
        return image;
    }


    @Override
    public String deleteImage(int id) {
        Image image = getImageById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(image);
            session.getTransaction().commit();
        }
        return null;
    }

    @Override
    public List<Image> getAdultImages() {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery("from Image where image_rating=3");
            return query.list();
        }
    }
}
