package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.DTOs.UserDTO_byIdUpd;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserByIdUpdMapper {

    UserRepository userRepository;

    public UserByIdUpdMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User dtoToObject(UserDTO_byIdUpd userByIdUpdDTO, int user_id) {
        User user = new User();
        User oldUserInfo;
        try {
            oldUserInfo = userRepository.getById(user_id);
        } catch (ObjectNotFoundException e) {
            throw new ObjectNotFoundException(e.getMessage());
        }
        //if a DTO field is null, use fields from old user info
        user.setUser_id(oldUserInfo.getUser_id());
        trySetFirstName(user, userByIdUpdDTO, oldUserInfo);
        trySetLastName(user, userByIdUpdDTO, oldUserInfo);
        trySetEmail(user, userByIdUpdDTO, oldUserInfo);
        user.setUsername((oldUserInfo.getUsername()));
        trySetPassword(user, userByIdUpdDTO, oldUserInfo);
        trySetRoleType(user, userByIdUpdDTO, oldUserInfo);
        user.setTotalPoints(oldUserInfo.getTotalPoints());
        user.setBadgeType();
        trySetStatus(user, userByIdUpdDTO, oldUserInfo);
        return user;
    }


    public void trySetFirstName(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getFirst_name() == null) {
            user.setFirst_name(oldUserInfo.getFirst_name());
        } else {
            user.setFirst_name(userDTO.getFirst_name());
        }
    }

    public void trySetLastName(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getLast_name() == null) {
            user.setLast_name(oldUserInfo.getLast_name());
        } else {
            user.setLast_name(userDTO.getLast_name());
        }
    }

    public void trySetEmail(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getEmail() == null) {
            user.setEmail(oldUserInfo.getEmail());
        } else {
            user.setEmail(userDTO.getEmail());
        }
    }

    public void trySetPassword(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getPassword() == null) {
            user.setPassword(oldUserInfo.getPassword());
        } else {
            user.setPassword(userDTO.getPassword());
        }
    }

    public void trySetRoleType(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getRoleType() == null) {
            user.setRoleType(oldUserInfo.getRoleType());
        } else {
            user.setRoleType(userDTO.getRoleType());
        }
    }


    public void trySetStatus(User user, UserDTO_byIdUpd userDTO, User oldUserInfo) {
        if (userDTO.getStatus() == null) {
            user.setStatus(oldUserInfo.getStatus());
        } else {
            user.setStatus(userDTO.getStatus());
        }
    }
}
