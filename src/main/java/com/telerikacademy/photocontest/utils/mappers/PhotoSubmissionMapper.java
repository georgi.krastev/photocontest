package com.telerikacademy.photocontest.utils.mappers;

import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.ContestDTO;
import com.telerikacademy.photocontest.models.DTOs.PhotoSubmissionDTO;
import com.telerikacademy.photocontest.repositories.contracts.CategoryRepository;
import com.telerikacademy.photocontest.repositories.contracts.ContestRepository;
import com.telerikacademy.photocontest.repositories.contracts.ImageRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class PhotoSubmissionMapper {
    private final ContestRepository contestRepository;
    private final CategoryRepository categoryRepository;
    private final ImageRepository imageRepository;


    public PhotoSubmissionMapper(ContestRepository contestRepository, CategoryRepository categoryRepository, ImageRepository imageRepository) {
        this.contestRepository = contestRepository;
        this.categoryRepository = categoryRepository;
        this.imageRepository = imageRepository;
    }

    public PhotoSubmission dtoToObject(PhotoSubmissionDTO photoSubmissionDTO, User user, Image image){
        PhotoSubmission photoSubmission = new PhotoSubmission();
        photoSubmission.setTitle(photoSubmissionDTO.getTitle());
        photoSubmission.setStory(photoSubmissionDTO.getStory());
        photoSubmission.setPhoto(image);
        photoSubmission.setCreation_time(LocalDateTime.now());
        photoSubmission.setUser(user);
        photoSubmission.setContest(contestRepository.getById(photoSubmissionDTO.getContest_id()));
        photoSubmission.setTotalScore(3);
        return photoSubmission;
    }
}
