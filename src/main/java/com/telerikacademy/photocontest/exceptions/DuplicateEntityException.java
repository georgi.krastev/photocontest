package com.telerikacademy.photocontest.exceptions;

public class DuplicateEntityException extends RuntimeException {
    public DuplicateEntityException(String type_of_object, String type_of_identifier, String concrete_identifier) {
        super(String.format("%s with %s %s already exists", type_of_object, type_of_identifier, concrete_identifier));
    }

    public DuplicateEntityException(String message) {
        super(message);
    }
}
