package com.telerikacademy.photocontest.exceptions;

public class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException(String type_of_object, String type_of_identifier, String concrete_identifier) {
        super(String.format("%s with %s %s does not exist.", type_of_object, type_of_identifier, concrete_identifier));
    }

    public ObjectNotFoundException(String fullMessage) {
        super(fullMessage);
    }

    public ObjectNotFoundException() {
        super("The object is not found");
    }
}
