package com.telerikacademy.photocontest.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "phone_numbers")
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phone_number_id")
    private int phone_id;

    @Column(name = "phone_number")
    private String phone_number;

    @OneToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    public PhoneNumber(int phone_id, String phone_number, User user) {
        this.phone_id = phone_id;
        this.phone_number = phone_number;
        this.user = user;
    }

    public PhoneNumber() {
    }

    public int getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(int phone_id) {
        this.phone_id = phone_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "phone_id=" + phone_id +
                ", phone_number='" + phone_number + '\'' +
                ", user=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return phone_id == that.phone_id && phone_number.equals(that.phone_number) && user.equals(that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone_id, phone_number, user);
    }
}
