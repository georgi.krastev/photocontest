package com.telerikacademy.photocontest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="photo_submission")

public class PhotoSubmission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_submission_id")
    private int photo_submission_id;

    @Column(name = "title")
    @NotNull
    private String title;

    @Column(name = "story")
    @NotNull
    private String story;

    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "creation_time")
    @NotNull
    private LocalDateTime creation_time;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image photo;

    @JsonIgnore
    @OneToMany(mappedBy = "photoSubmission", fetch = FetchType.EAGER)
    private Set<Comment> comments;

    @JsonIgnore
    @OneToMany(mappedBy = "photoSubmission", fetch = FetchType.EAGER)
    private Set<Score> scores;

    @JsonIgnore
    @Column(name = "total_score")
    private long totalScore;

    public PhotoSubmission(int photo_submission_id,
                           String title,
                           String story,
                           Contest contest,
                           User user,
                           Image photo,
                           Set<Comment> comments,
                           Set<Score> scores) {
        this.photo_submission_id = photo_submission_id;
        this.title = title;
        this.story = story;
        this.contest = contest;
        this.user = user;
        this.photo = photo;
        this.comments = comments;
        this.scores = scores;
    }

    public PhotoSubmission() {

    }


    public int getPhoto_submission_id() {
        return photo_submission_id;
    }

    public void setPhoto_submission_id(int photo_submission_id) {
        this.photo_submission_id = photo_submission_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(LocalDateTime creation_time) {
        this.creation_time = creation_time;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }
    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(long totalScore) {
        this.totalScore = totalScore;
    }
}
