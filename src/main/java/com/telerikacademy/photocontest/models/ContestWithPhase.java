package com.telerikacademy.photocontest.models;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class ContestWithPhase {
private int contest_id;
private String title;
private CategoryType categoryType;
private LocalDateTime creationTime;
private Boolean is_finished;
private User organizer_id;
private Image photo;
private PhaseType phaseType;

    public ContestWithPhase(int contest_id,
                            String title,
                            CategoryType categoryType,
                            LocalDateTime creationTime,
                            Boolean is_finished,
                            User organizer_id,
                            Image photo,
                            PhaseType phaseType) {
        this.contest_id = contest_id;
        this.title = title;
        this.categoryType = categoryType;
        this.creationTime = creationTime;
        this.is_finished = is_finished;
        this.organizer_id = organizer_id;
        this.photo = photo;
        this.phaseType = phaseType;
    }

    public ContestWithPhase() {
    }

    public int getContest_id() {
        return contest_id;
    }

    public void setContest_id(int contest_id) {
        this.contest_id = contest_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Boolean getIs_finished() {
        return is_finished;
    }

    public void setIs_finished(Boolean is_finished) {
        this.is_finished = is_finished;
    }

    public User getOrganizer_id() {
        return organizer_id;
    }

    public void setOrganizer_id(User organizer_id) {
        this.organizer_id = organizer_id;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }

    public PhaseType getPhaseType() {
        return phaseType;
    }

    public void setPhaseType(PhaseType phaseType) {
        this.phaseType = phaseType;
    }
}
