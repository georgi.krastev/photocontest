package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="images")

public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    private int image_id;

    @Column(name = "image_link")
    @NotNull
    @Size(max = 132, message = "Maximum is 132 symbols")
    private String image_link;

    @Column(name = "image_rating")
    private Integer image_rating;

    public Image(int image_id, String post_title, Integer image_rating) {
        this.image_id = image_id;
        this.image_link = post_title;
        this.image_rating = image_rating;
    }

    public Image() {

    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public Integer getImage_rating() {
        return image_rating;
    }

    public void setImage_rating(Integer image_rating) {
        this.image_rating = image_rating;
    }
    
}
