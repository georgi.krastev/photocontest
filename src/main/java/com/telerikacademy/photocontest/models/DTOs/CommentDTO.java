package com.telerikacademy.photocontest.models.DTOs;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Component
public class CommentDTO {

    @NotNull(message = "Comment cannot be null.")
    private String content;

    @NotNull(message = "PhotoSubmission id cannot be null.")
    private Integer photoSubmission_id;

    public CommentDTO() {
    }

    public CommentDTO(String content, Integer photoSubmission_id) {
        this.content = content;
        this.photoSubmission_id = photoSubmission_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getPhotoSubmission_id() {
        return photoSubmission_id;
    }

    public void setPhotoSubmission_id(Integer photoSubmission_id) {
        this.photoSubmission_id = photoSubmission_id;
    }
    
}
