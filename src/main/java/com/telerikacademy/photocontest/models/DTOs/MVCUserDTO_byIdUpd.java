package com.telerikacademy.photocontest.models.DTOs;

import com.telerikacademy.photocontest.models.BadgeType;
import com.telerikacademy.photocontest.models.RoleType;
import com.telerikacademy.photocontest.models.Status;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MVCUserDTO_byIdUpd {
//shouldn't change userID and username
    private int user_id;
    @Size(min = 4, max = 32, message = "Minimum first name length is 4 symbols, maximum first name length is 32 symbols")
    private String first_name;

    @Size(min = 4, max = 32, message = "Minimum last name length is 4 symbols, maximum first name length is 32 symbols")
    private String last_name;

    @Pattern(regexp = "^(.+)@(.+)$", message = "Email should contain @")
    private String email;

    private String username;
    @Size(min = 4, message = "Minimum password length is 4 symbols")
    private String password;
    private RoleType roleType;
    private Status status;

    public MVCUserDTO_byIdUpd(int user_id,
                              String first_name,
                              String last_name,
                              String email,
                              String password,
                              String roleTypeName,
                              String statusString) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.roleType = RoleType.valueOf(roleTypeName);
        this.status = Status.valueOf(statusString);
    }

    public MVCUserDTO_byIdUpd(int user_id, String first_name,
                              String last_name,
                              String email,
                              String password,
                              RoleType roleType,
                              Status status) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.password = password;
        this.roleType = roleType;
        this.status = status;
    }

    public MVCUserDTO_byIdUpd() {
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
