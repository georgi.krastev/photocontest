package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "statuses")
public class Status {
    public static final Map<String, Status> statuses = new HashMap<>();
    public static final Status ACTIVE = new Status(1, "Active");
    public static final Status BLOCKED = new Status(2, "Blocked");
    public static final Status DELETED = new Status(3, "Deleted");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_id")
    private int status_id;

    @Column
    @NotNull
    @Size(max = 132, message = "Maximum is 132 symbols")
    private String status_name;

    public Status(int status_id, String status_name) {
        this.status_id = status_id;
        this.status_name = status_name;
        statuses.put(status_name, this);
    }

    public Status() {
    }

    public static Status valueOf(String statusName){
        return statuses.get(statusName);
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    @Override
    public String toString() {
        return "Status{" +
                "status_id=" + status_id +
                ", status_name='" + status_name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Status status = (Status) o;
        return status_id == status.status_id && status_name.equals(status.status_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status_id);
    }
}
