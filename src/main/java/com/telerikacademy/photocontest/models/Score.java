package com.telerikacademy.photocontest.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="scores")
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "score_id")
    private int scoreId;

    @Column(name = "score")
    @NotNull
    private int score;

    @ManyToOne
    @JoinColumn(name = "photo_id")
    private PhotoSubmission photoSubmission;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Score() {
    }

    public Score(int scoreId,
                 int score,
                 PhotoSubmission photoSubmission,
                 User user) {
        this.scoreId = scoreId;
        this.score = score;
        this.photoSubmission = photoSubmission;
        this.user = user;
    }

    public int getScoreId() {
        return scoreId;
    }

    public void setScoreId(int scoreId) {
        this.scoreId = scoreId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public PhotoSubmission getPhotoSubmission() {
        return photoSubmission;
    }

    public void setPhotoSubmission(PhotoSubmission photoSubmission) {
        this.photoSubmission = photoSubmission;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
