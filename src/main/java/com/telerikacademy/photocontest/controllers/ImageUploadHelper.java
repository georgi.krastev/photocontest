package com.telerikacademy.photocontest.controllers;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
public class ImageUploadHelper {

    private static ImageService imageService;
    private static UserService userService;

    @Autowired
    public ImageUploadHelper(ImageService imageService) {
        this.imageService = imageService;
    }

    public static Image uploadCloudinary(MultipartFile multipartFile, User user) throws IOException {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dsrlmhn6d",
                "api_key", "615371959246383",
                "api_secret", "B2g49RM3MAidTHOQoidmJ7gAuNo",
                "secure", true));
        Map upload_result = null;
        upload_result = cloudinary.uploader().cloudinary().uploader().upload(multipartFile.getBytes(),ObjectUtils.emptyMap());
        Image image = new Image();
        image.setImage_link(upload_result.get("url").toString());
        Integer rating = Integer.valueOf(ModerationHelper.moderateText(upload_result.get("url").toString()));
        image.setImage_rating(rating);
        imageService.createImage(image);
        return image;
    }
}