package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.services.contracts.ContestPhaseService;
import com.telerikacademy.photocontest.services.contracts.ContestService;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@RequestMapping("/")
@Controller
public class HomeMvcController {

    private final ImageService imageService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestPhaseService contestPhaseService;
    private final ContestService contestService;

    @Autowired
    public HomeMvcController(ImageService imageService, UserService userService, AuthenticationHelper authenticationHelper, ContestPhaseService contestPhaseService, ContestService contestService) {
        this.imageService = imageService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.contestPhaseService = contestPhaseService;
        this.contestService = contestService;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        String numberOfUsers = String.valueOf(userService.getNumberOfUsers());
        model.addAttribute("numberOfUsers", numberOfUsers);
        boolean isLoggedIn = authenticationHelper.isLoggedIn(session);
        model.addAttribute("isLoggedIn", isLoggedIn);
        checkFinishedContests();
        return "index";
    }
    @Scheduled(fixedRate = 100000)
    public void checkFinishedContests(){
       contestService.updateFinishedContests();
    }

}
