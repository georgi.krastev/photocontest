package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.Utility;
import com.telerikacademy.photocontest.models.DTOs.LoginDTO;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@Controller
public class ForgotPasswordController {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private UserService userService;

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        try {
            userService.updateResetPasswordToken(token, email);
            String resetPasswordLink = Utility.getSiteURL(request) + "/reset_password?token=" + token;
            sendEmail(email, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (EntityNotFoundException ex) {
            model.addAttribute("message", ex.getMessage());
        } catch (UnsupportedEncodingException | MessagingException e) {
            model.addAttribute("message", "Error while sending email");
        }

        return "forgot_password_form";
    }

    public void sendEmail(String recipientEmail, String link)
            throws MessagingException, UnsupportedEncodingException {


        //--------------
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(recipientEmail);
        helper.setText("How are you?");
        String subject = "Here's the link to reset your password";
        helper.setSubject(subject);

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";


        helper.setText(content, true);

        mailSender.send(message);

    }

    @GetMapping("/reset_password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model, HttpSession session) {
        User user;
        try{
            user = userService.getByResetPasswordToken(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "User not found");
            model.addAttribute("loginDTO", new LoginDTO());
            return "login";
        } catch (ArrayIndexOutOfBoundsException e){
            model.addAttribute("message", "User not found");
            model.addAttribute("loginDTO", new LoginDTO());
            return "login";
        }

        model.addAttribute("token", token);
        session.setAttribute("token",token);
        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            model.addAttribute("loginDTO", new LoginDTO());
            return "login";
        }

        return "reset_password_form";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model,HttpSession session) {
        String token = (String) session.getAttribute("token");
        String password = request.getParameter("password");
        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");
        model.addAttribute("loginDTO", new LoginDTO());

        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            model.addAttribute("loginDTO", new LoginDTO());
            return "login";
        } else {
            userService.updatePassword(user, password);
            model.addAttribute("message", "You have successfully changed your password.");
        }
        return "login";
    }
}
