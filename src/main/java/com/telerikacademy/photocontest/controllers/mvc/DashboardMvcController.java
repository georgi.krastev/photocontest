package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.models.CategoryType;
import com.telerikacademy.photocontest.models.DTOs.FilterContestDto;
import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.models.enums.ContestSortOptions;
import com.telerikacademy.photocontest.services.contracts.CategoryService;
import com.telerikacademy.photocontest.services.contracts.ContestPhaseService;
import com.telerikacademy.photocontest.services.contracts.ContestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping
public class DashboardMvcController {

    private final ContestService contestService;
    private final CategoryService categoryService;
    private final ContestPhaseService contestPhaseService;
    private final AuthenticationHelper authenticationHelper;

    public DashboardMvcController(ContestService contestService, CategoryService categoryService, ContestPhaseService contestPhaseService, AuthenticationHelper authenticationHelper) {
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.contestPhaseService = contestPhaseService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("categories")
    public List<CategoryType> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("contestSortOptions")
    public ContestSortOptions[] populateContestSortOptions() {
        return ContestSortOptions.values();
    }

    @ModelAttribute("phases")
    public List<PhaseType> populatePhases() {
        return contestPhaseService.getPhaseTypes();
    }

    @GetMapping("/regular-dashboard")
    public String showRegularUserDashBoard(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        model.addAttribute("runningContests", contestService.getRunningContestsByParticipant(user.getUser_id()));
        model.addAttribute("finishedContests", contestService.getFinishedContestsByParticipant(user.getUser_id()));
        model.addAttribute("user", user);
        int badge_id = user.getBadgeType().getBadge_id();
        int minForNextLevel = User.MAX_FOR_LEVELS[badge_id] + 1;
        int remainingPoints = minForNextLevel - user.getTotalPoints();
        model.addAttribute("remainingPoints", remainingPoints);
        return "regular-dashboard";
    }

    @GetMapping("/all-phase1-contests-filter")
    public String showAllPhase1Contests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        var allContestsInPhase1 = contestService.getAll(Optional.empty(),
                Optional.empty(), Optional.of(1), Optional.empty());
        model.addAttribute("user", user);
        model.addAttribute("contests", allContestsInPhase1);
        model.addAttribute("filterContestDto", new FilterContestDto());
        return "all-contests-in-phase-1";
    }

    @PostMapping("/all-phase1-contests-filter")
    public String filterAllPhase1Contests(@ModelAttribute("filterContestDto") FilterContestDto filterContestDto,
                                          HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        filterContestDto.setPhaseId(1);
        var filtered = contestService.getAll(
                Optional.ofNullable(filterContestDto.getTitle().isBlank() ? null : filterContestDto.getTitle()),
                Optional.ofNullable(filterContestDto.getCategoryId() == null ? null : filterContestDto.getCategoryId()),
                Optional.ofNullable(filterContestDto.getPhaseId() == null ? null : filterContestDto.getPhaseId()),
                Optional.ofNullable(filterContestDto.getContestSortOption() == null ?
                        null : ContestSortOptions.valueOfPreview(filterContestDto.getContestSortOption())));
        model.addAttribute("user", user);
        model.addAttribute("contests", filtered);
        return "all-contests-in-phase-1";
    }


}


