package com.telerikacademy.photocontest.controllers.mvc;

import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.exceptions.*;
import com.telerikacademy.photocontest.models.*;
import com.telerikacademy.photocontest.models.DTOs.*;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.RoleTypeService;
import com.telerikacademy.photocontest.services.contracts.StatusService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.utils.mappers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class UserMvcController {

    private final ImageService imageService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserCreateMapper userCreateMapper;
    private final UserByIdUpdMapper userByIdUpdMapper;
    private final SelfUpdateMapper selfUpdateMapper;
    private final MVCUserByIdUpdMapper mVCUserByIdUpdMapper;
    private final RoleTypeService roleTypeService;
    private final StatusService statusService;

    @Autowired
    public UserMvcController(ImageService imageService,
                             UserService userService,
                             AuthenticationHelper authenticationHelper,
                             UserCreateMapper userCreateMapper,
                             UserByIdUpdMapper userByIdUpdMapper,
                             SelfUpdateMapper selfUpdateMapper,
                             MVCUserByIdUpdMapper mVCUserByIdUpdMapper, RoleTypeService roleTypeService, StatusService statusService) {
        this.imageService = imageService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userCreateMapper = userCreateMapper;
        this.userByIdUpdMapper = userByIdUpdMapper;
        this.selfUpdateMapper = selfUpdateMapper;
        this.mVCUserByIdUpdMapper = mVCUserByIdUpdMapper;
        this.roleTypeService = roleTypeService;
        this.statusService = statusService;
    }

    @ModelAttribute("roles")
    public List<RoleType> populateRoles(){
        return roleTypeService.getAll();
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses(){
        return statusService.getAll();
    }

    @GetMapping("/list_users")
    public String showUserList(HttpSession session,
                               Model model) {
//        TODO delete = null?
        User initiator = null;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("userSearch", new UserSearch());
        try {
            model.addAttribute("all_users",
                    userService.getAll(Optional.empty(), initiator));
            return "users_list";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error_access_denied", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/list_users")
    public String showUserList(@ModelAttribute("userSearch") UserSearch userSearch,
                               HttpSession session,
                               Model model) {
        //        TODO delete = null?
        User initiator = null;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        Optional<String> search = Optional.ofNullable(userSearch.getSearch());
        try {
            model.addAttribute("all_users",
                    userService.getAll(search, initiator));

            return "users_list";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error_access_denied", e.getMessage());
            return "access-denied";
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }


    @GetMapping("/deleteuser")
    public String deleteUserForm(HttpSession session, @RequestParam int id, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        try {
            userService.delete(id, user);
            return "redirect:/list_users";
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @GetMapping("/makeadmin")
    public String makeUserAdmin(HttpSession session, @RequestParam int id, Model model) {
        return adminHelper(RoleType.valueOf("Admin"), session, id, model);
    }

    @GetMapping("/removeadmin")
    public String removeAdminRights(HttpSession session, @RequestParam int id, Model model) {
        return adminHelper(RoleType.valueOf("Organizer"), session, id, model);
    }

    @GetMapping("/makeorganizer")
    public String makeUserOrganizer(HttpSession session, @RequestParam int id, Model model) {
        return adminHelper(RoleType.valueOf("Organizer"), session, id, model);
    }

    @GetMapping("/removeorganizer")
    public String removeOrganizerRights(HttpSession session, @RequestParam int id, Model model) {
        return adminHelper(RoleType.valueOf("User"), session, id, model);
    }

    public String adminHelper(RoleType desiredRole, HttpSession session, int id, Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        User user_update = userService.getById(id, initiator);
        user_update.setRoleType(desiredRole);
        userService.updateById(initiator, user_update);
        return "redirect:/list_users";
    }

    @GetMapping("/blockuser")
    public String blockUser(HttpSession session, @RequestParam int id, Model model) {
        return activationHelper(Status.BLOCKED, session, id, model);
    }

    @GetMapping("/unblockuser")
    public String unblockUser(HttpSession session, @RequestParam int id, Model model) {
        return activationHelper(Status.ACTIVE, session, id, model);
    }

    public String activationHelper(Status desiredStatus, HttpSession session, int id, Model model) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        User user_update = userService.getById(id, initiator);
        user_update.setStatus(desiredStatus);
        userService.updateById(initiator, user_update);
        return "redirect:/list_users";
    }

    @GetMapping("/updateme")
    public String showUpdateSelfUserForm(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }

        SelfUpdDTO userDTO = selfUpdateMapper.objectToDTO(user);
        model.addAttribute("userDTO", userDTO);
        return "self-update";
    }

    @PostMapping("/updateme")
    public String updateme(@Valid @ModelAttribute("userDTO") SelfUpdDTO userDTO,
                           BindingResult errors,
                           Model model,
                           HttpSession session) {
        if (errors.hasErrors()) {
            return "self-update";
        }
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        User user = selfUpdateMapper.dtoToObject(userDTO, initiator);
        try {
            userService.updateSelf(user);
        } catch (DuplicateEntityException e) {
            model.addAttribute("duplicate_email", e.getMessage());
            return "duplicate-email";
        }
        return "self-update";
    }

    @GetMapping("/updatemypassword")
    public String showMyPasswordUpdForm(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        model.addAttribute("newPasswordDTO", new NewPasswordDTO());
        return "update-own-password";
    }

    @PostMapping("/updatemypassword")
    public String updateMyPassword(@Valid @ModelAttribute("newPasswordDTO") NewPasswordDTO newPasswordDTO,
                                   BindingResult errors,
                                   Model model,
                                   HttpSession session) {
        if (errors.hasErrors()) {
            return "update-own-password";
        }
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        if (newPasswordDTO.getNewPassword().equals(newPasswordDTO.getNewPasswordConfirmation())) {
            initiator.setPassword(newPasswordDTO.getNewPassword());
            userService.updateSelf(initiator);
        } else {
            errors.rejectValue("newPasswordConfirmation", "inputError",
                    "Password and password confirmation do not match.");
            return "update-own-password";
        }
        return "password-update-success";
    }

    @GetMapping("/updateuser")
    public String showUpdateUserForm(Model model, HttpSession session, int id) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }

        MVCUserDTO_byIdUpd userDTO;
        try {
            userDTO = mVCUserByIdUpdMapper.objectToDTO(id);
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        model.addAttribute("userUpdDTO", userDTO);
        return "user-update";
    }

    @PostMapping("/updateuser")
    public String updateUser(@Valid @ModelAttribute("userUpdDTO") MVCUserDTO_byIdUpd userDTO,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        if (errors.hasErrors()) {
            return "user-update";
        }
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        User user;
        try {
            user = mVCUserByIdUpdMapper.dtoToObject(userDTO);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d does not exist",
                    userDTO.getUser_id()));
        }
        try {
            userService.updateById(initiator, user);
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        } catch (DuplicateEntityException e) {
            model.addAttribute("duplicate_email", e.getMessage());
            return "duplicate-email";
        }
        return "user-update";
    }

    @GetMapping("/updatepasswordbyid")
    public String showPasswordByIdUpdForm(Model model, HttpSession session, int id) {
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        String username;
        try {
            username = userService.getById(id, initiator).getUsername();
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

        ByIdNewPasswordDTO newPasswordDTO = new ByIdNewPasswordDTO();
        newPasswordDTO.setUsername(username);
        model.addAttribute("byIdNewPasswordDTO", newPasswordDTO);
        return "update-password-by-id";
    }

    @PostMapping("/updatepasswordbyid")
    public String updatePasswordById(@Valid @ModelAttribute("byIdNewPasswordDTO") ByIdNewPasswordDTO newPasswordDTO,
                                     BindingResult errors,
                                     Model model,
                                     HttpSession session) {
        if (errors.hasErrors()) {
            return "update-password-by-id";
        }
        User initiator;
        try {
            initiator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        User user;
        try {
            user = userService.getByUsername(newPasswordDTO.getUsername());
        } catch (ObjectNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        if (newPasswordDTO.getNewPassword().equals(newPasswordDTO.getNewPasswordConfirmation())) {
            user.setPassword(newPasswordDTO.getNewPassword());
            userService.updateById(initiator, user);
        } else {
            errors.rejectValue("newPasswordConfirmation", "inputError",
                    "Password and password confirmation do not match.");
            return "update-password-by-id";
        }
        return "password-update-success";
        }




}
