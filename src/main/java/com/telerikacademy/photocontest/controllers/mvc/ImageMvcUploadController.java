package com.telerikacademy.photocontest.controllers.mvc;

import com.cloudinary.utils.ObjectUtils;
import com.telerikacademy.photocontest.controllers.*;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import com.telerikacademy.photocontest.controllers.AuthenticationHelper;
import com.telerikacademy.photocontest.controllers.FileUploadUtil;
import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.models.Image;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.ImageService;
import com.telerikacademy.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import com.cloudinary.Cloudinary;



@Controller
public class ImageMvcUploadController {


    private final ImageService imageService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ImageMvcUploadController(ImageService imageService, UserService userService, AuthenticationHelper authenticationHelper) {
        this.imageService = imageService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/upload")
    public String test(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        return "upload_photo";
    }

    @PostMapping("/images/save")
    public String saveImage(HttpSession session, @RequestParam("image") MultipartFile multipartFile, Model model) throws IOException {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            model.addAttribute("auth_error", e.getMessage());
            return "redirect:/login";
        }
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        Image image0;
        try {
            image0 = imageService.getImageByUserId(user.getUser_id());
            imageService.deleteImage(image0.getImage_id());
        } catch(NullPointerException e){
            System.out.print("No image found");
        } finally {

            String file_link = "images/user-photos/" + user.getUser_id() + "/" + fileName;

            Image image = ImageUploadHelper.uploadCloudinary(multipartFile,user);
            user.setPhoto(image);
            userService.updateSelf(user);
            session.setAttribute("pictureLink",image.getImage_link());

            String uploadDir = "src/main/resources/static/images/user-photos/" + user.getUser_id();

            FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

            return "redirect:/";
        }
    }
}
