package com.telerikacademy.photocontest.controllers;

import com.telerikacademy.photocontest.exceptions.AuthenticationFailureException;
import com.telerikacademy.photocontest.exceptions.ObjectNotFoundException;
import com.telerikacademy.photocontest.models.Status;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HTTP_HEADER = "Authorization";
    public UserService service;

    @Autowired
    public AuthenticationHelper(UserService service) {
        this.service = service;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HTTP_HEADER)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Authentication required.");
        }
        User identifiedUser;
        try {
            String username = headers.getFirst(AUTHORIZATION_HTTP_HEADER);
            identifiedUser = service.getByUsername(username);
            if (identifiedUser.getStatus().equals(Status.DELETED)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
            }
            return identifiedUser;
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public User tryGetUser(HttpSession session) {
        //TODO ask question why and how to understand that the expression below won't throw NullPointerException?
        String username = (String) session.getAttribute("currentUser");

        if (username == null) {
            throw new AuthenticationFailureException("No user is logged in.");
        }
        return service.getByUsername(username);
    }

    public boolean isLoggedIn(HttpSession session) {
        try {
            tryGetUser(session);
            return true;
        } catch (AuthenticationFailureException e) {
            return false;
        }

    }

    public User verifyAuthentication(String username, String password) {
        User existingUser;
        try {
            existingUser = service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException("There is no user with such username. To register, use registration button.");
        }
        if (!existingUser.getPassword().equals(password) || existingUser.getStatus().equals(Status.DELETED)) {
            throw new AuthenticationFailureException("Wrong username or password.");
        }
        return existingUser;
    }

//    TODO Georgi pls check if this can be deleted
    public User validateOwnership(HttpHeaders headers, int id) {
        try {
            User owner = tryGetUser(headers);
            int owner_id = owner.getUser_id();
            if (owner_id != id) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You are not the owner of the item");
            }
            return owner;
        } catch (ResponseStatusException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You need to log in to update user information");
        }
    }


}
