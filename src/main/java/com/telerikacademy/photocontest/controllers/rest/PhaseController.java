package com.telerikacademy.photocontest.controllers.rest;

import com.telerikacademy.photocontest.models.PhaseType;
import com.telerikacademy.photocontest.services.contracts.PhaseTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/phases")
public class PhaseController {

    private final PhaseTypeService phaseTypeService;

    @Autowired
    public PhaseController(PhaseTypeService phaseTypeService) {
        this.phaseTypeService = phaseTypeService;
    }

    @GetMapping
    public List<PhaseType> populatePhases() {
        return phaseTypeService.getPhaseTypes();
    }
}
