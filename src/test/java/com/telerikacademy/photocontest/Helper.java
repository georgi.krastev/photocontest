package com.telerikacademy.photocontest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.photocontest.models.*;

import java.time.LocalDateTime;
import java.util.HashSet;

import static com.telerikacademy.photocontest.utils.mappers.DeletedUserMapper.*;

public class Helper {

    public static Contest createMockContest(){
        var mockContest = new Contest();
        mockContest.setContest_id(1);
        mockContest.setTitle("Let's go far");
        mockContest.setCategoryType(CategoryType.valueOf(1));
        mockContest.setCreation_time(LocalDateTime.now());
        mockContest.setIs_finished(Boolean.FALSE);
        mockContest.setOrganizer_id(createMockActiveOrganizer());
        HashSet<User> juries = new HashSet<>();
        juries.add(createMockActiveOrganizer());
        juries.add(createMockActiveAdmin());
        mockContest.setJurors(new HashSet<>());
        HashSet <User> participants = new HashSet<>();
        mockContest.setParticipants(participants);
        mockContest.setPhoto(createMockImage());
        mockContest.setFirstPlaceWinners(new HashSet<PhotoSubmission>());
        mockContest.setSecondPlaceWinners(new HashSet<PhotoSubmission>());
        mockContest.setThirdPlaceWinners(new HashSet<PhotoSubmission>());
        return mockContest;
    }

    public static ContestWithPhase createMockContestWithPhase(){
        var mockContestWithPhase = new ContestWithPhase();
        mockContestWithPhase.setContest_id(1);
        mockContestWithPhase.setTitle("Let's go far");
        mockContestWithPhase.setCategoryType(CategoryType.valueOf(1));
        mockContestWithPhase.setCreationTime(LocalDateTime.now());
        mockContestWithPhase.setIs_finished(Boolean.FALSE);
        mockContestWithPhase.setOrganizer_id(createMockActiveOrganizer());
        mockContestWithPhase.setPhoto(createMockImage());
        mockContestWithPhase.setPhaseType(PhaseType.valueOf(1));
        return mockContestWithPhase;
    }

    public static User createMockActiveAdmin() {
        var mockUser = new User();
        mockUser.setUser_id(1);
        mockUser.setFirst_name("Penda");
        mockUser.setLast_name("Pendova");
        mockUser.setUsername("penda");
        mockUser.setPassword("pendaRules!");
        mockUser.setEmail("penda@forum.com");
        mockUser.setRoleType(RoleType.valueOf("Admin"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.ACTIVE);
        return mockUser;
    }


    public static User createMockActiveRegUser() {
        var mockUser = new User();
        mockUser.setUser_id(2);
        mockUser.setFirst_name("Pizho");
        mockUser.setLast_name("Pizhev");
        mockUser.setUsername("pizho");
        mockUser.setPassword("pizhoRules!");
        mockUser.setEmail("pizho1@forum.com");
        mockUser.setRoleType(RoleType.valueOf("User"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.ACTIVE);
        return mockUser;
    }

    public static User createMockBlockedAdmin() {
        var mockUser = new User();
        mockUser.setUser_id(3);
        mockUser.setFirst_name("Penda");
        mockUser.setLast_name("Pendova");
        mockUser.setUsername("penda");
        mockUser.setPassword("pendaRules!");
        mockUser.setEmail("penda@forum.com");
        mockUser.setRoleType(RoleType.valueOf("Admin"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.BLOCKED);
        return mockUser;
    }

    public static User createMockBlockedRegUser() {
        var mockUser = new User();
        mockUser.setUser_id(4);
        mockUser.setFirst_name("Pizho");
        mockUser.setLast_name("Pizhev");
        mockUser.setUsername("pizho1");
        mockUser.setPassword("pizhoRules!");
        mockUser.setEmail("pizho1@forum.com");
        mockUser.setRoleType(RoleType.valueOf("User"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.BLOCKED);
        return mockUser;
    }

    public static User createMockDeletedRegUser() {
        var mockUser = new User();
        mockUser.setUser_id(4);
        mockUser.setFirst_name(DELETED_NAME);
        mockUser.setLast_name(DELETED_SURNAME);
        mockUser.setUsername("pizho"+DELETED_USERNAME_ADDITION);
        mockUser.setPassword(DELETED_PASSWORD);
        mockUser.setEmail("pizho@forum.com"+ DELETED_EMAIL_ADDITION);
        mockUser.setRoleType(RoleType.valueOf("User"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.DELETED);
        return mockUser;
    }


    public static User createMockActiveOrganizer() {
        var mockUser = new User();
        mockUser.setUser_id(5);
        mockUser.setFirst_name("Easter");
        mockUser.setLast_name("Bunny");
        mockUser.setUsername("bunny");
        mockUser.setPassword("holidayTime!");
        mockUser.setEmail("bunny@forum.com");
        mockUser.setRoleType(RoleType.valueOf("Organizer"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.ACTIVE);
        return mockUser;
    }

    public static User createMockBlockedOrganizer() {
        var mockUser = new User();
        mockUser.setUser_id(6);
        mockUser.setFirst_name("Workaholic");
        mockUser.setLast_name("Bunny");
        mockUser.setUsername("bunny");
        mockUser.setPassword("workwork!");
        mockUser.setEmail("work@forum.com");
        mockUser.setRoleType(RoleType.valueOf("Organizer"));
        mockUser.setBadgeType(BadgeType.valueOf("Junkie"));
        mockUser.setStatus(Status.BLOCKED);
        return mockUser;
    }

    public static Image createMockImage(){
        var mockImage = new Image();
        mockImage.setImage_id(1);
        mockImage.setImage_link("test");
        return mockImage;
    }

    /**
     * Accepts an object and returns the stringified object.
     * Useful when you need to pass a body to a HTTP request.
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static CategoryType createMockCategory(){
        return new CategoryType(10,"Leisure");
    }

    public static Comment createMockComment(){
        return new Comment(1,"Beautiful",createMockSubmission(),createMockActiveOrganizer(),LocalDateTime.now());
    }

    public static PhotoSubmission createMockSubmission(){
        return new PhotoSubmission(1, "My world view","The world without glasses is blurred",
               createMockContest(),createMockActiveRegUser(),createMockImage(),new HashSet<Comment>(), new HashSet<Score>());

    }

    public static Score createMockScore(){
        PhotoSubmission photoSubmission = createMockSubmission();
        Score score = new Score(1,9,photoSubmission,createMockActiveOrganizer());
        HashSet<Score> scoresWithThis = new HashSet<>();
        scoresWithThis.add(score);
        photoSubmission.setScores(scoresWithThis);
        return score;
    }


    public static ContestPhase createMockContestPhase(){
        ContestPhase contestPhase = new ContestPhase(1,PhaseType.PHASE1,
                createMockContest(),LocalDateTime.of(2022,4,20,9,30),LocalDateTime.of(2022,4,23,9,30));
        return contestPhase;
    }
}
