package com.telerikacademy.photocontest;

import com.telerikacademy.photocontest.exceptions.*;
import com.telerikacademy.photocontest.models.User;
import com.telerikacademy.photocontest.repositories.contracts.UserRepository;
import com.telerikacademy.photocontest.services.UserServiceImpl;
import com.telerikacademy.photocontest.utils.mappers.DeletedUserMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
public class UserTests {

    @Mock
    UserRepository userMockRepository;

    @Mock
    DeletedUserMapper deletedMockUserMapper;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getAll_should_callRepository_when_initiatorIsActiveAdmin_and_searchIsEmpty() {
        //Arrange
        User user = Helper.createMockActiveAdmin();
        Mockito.when(userMockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        userService.getAll(Optional.empty(), user);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_should_callRepository_when_initiatorIsActiveOrganizer_and_searchIsEmpty() {
        //Arrange
        User user = Helper.createMockActiveOrganizer();
        Mockito.when(userMockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        userService.getAll(Optional.empty(), user);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_should_returnListOfMatches_when_matchExists() {
        String search = "le";
        User admin = Helper.createMockActiveAdmin();
        User mockUser2 = Helper.createMockActiveRegUser();
        User mockUser3 = Helper.createMockActiveOrganizer();
        User mockUser4 = Helper.createMockBlockedRegUser();
        mockUser2.setFirst_name("A" + search + "x");
        mockUser3.setFirst_name("A" + search + "k");
        List<User> userList = new ArrayList<>();
        userList.add(mockUser2);
        userList.add(mockUser3);
        userList.add(mockUser4);
        Mockito.when(userMockRepository.search(search)).thenReturn(userList
                .stream()
                .filter(user -> user.getFirst_name().contains(search))
                .collect(Collectors.toList()));


        List<User> result = userService.getAll(Optional.of(search), admin);

        Assertions.assertTrue(result.size() == 2);
        for (User user :
                result) {
            Assertions.assertTrue(user.getFirst_name().contains(search));
        }

    }
    @Test
    public void getAll_should_throw_when_initiatorIsBlockedOrganizer_and_userIdExists(){
        User initiator = Helper.createMockBlockedOrganizer();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getAll(Optional.empty(),initiator));
    }

    @Test
    public void getUsers_should_throwException_when_initiatorIsRegularUser() {
        User user = Helper.createMockActiveRegUser();
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getAll(Optional.empty(), user));
    }

    @Test
    public void getById_should_returnUser_when_initiatorIsActiveAdmin_and_userIdExists(){
        User initiator = Helper.createMockActiveAdmin();
        User user = Helper.createMockActiveRegUser();
        int id = user.getUser_id();
        Mockito.when(userMockRepository.getById(id)).thenReturn(user);

        User returned = userService.getById(id,initiator);

        Assertions.assertEquals(user, returned);
    }

    @Test
    public void getById_should_returnUser_when_initiatorIsActiveOrganizer_and_userIdExists(){
        User initiator = Helper.createMockActiveOrganizer();
        User user = Helper.createMockActiveRegUser();
        int id = user.getUser_id();
        Mockito.when(userMockRepository.getById(id)).thenReturn(user);

        User returned = userService.getById(id,initiator);

        Assertions.assertEquals(user, returned);
    }

    @Test
    public void getById_should_throw_when_initiatorIsBlockedOrganizer_and_userIdExists(){
        User initiator = Helper.createMockBlockedOrganizer();
        User user = Helper.createMockActiveRegUser();
        int id = user.getUser_id();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getById(id,initiator));
    }

    @Test
    public void getById_should_throwException_when_initiatorIsRegularUser() {
        User user = Helper.createMockActiveRegUser();
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getById(1, user));
    }

    @Test
    public void getByUsername_should_callRepository(){
        String username = "username1";
        userService.getByUsername(username);
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getUniqueByField("username", username);
    }

    @Test
    public void create_should_callRepositoryCreateMethod_when_usernameAndEmailUnique() {
        User mockNewUser = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.getUniqueByField("username",mockNewUser.getUsername())).thenThrow(ObjectNotFoundException.class);
        Mockito.when(userMockRepository.getUniqueByField("email",mockNewUser.getEmail())).thenThrow(ObjectNotFoundException.class);

        //Act
        userService.create(mockNewUser);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).create(mockNewUser);
    }

    @Test
    public void create_should_throw_when_sameUsernameFound(){
        String existingUsername = "existingUsername";
        User mockExistingUser = Helper.createMockActiveRegUser();
        User mockNewUser = Helper.createMockActiveOrganizer();
        mockExistingUser.setUsername(existingUsername);
        mockNewUser.setUsername(existingUsername);
        Mockito.when(userMockRepository.getUniqueByField("username", existingUsername)).thenReturn(mockExistingUser);

        Assertions.assertThrows(DuplicateUsernameException.class, () -> userService.create(mockNewUser));

    }

    @Test
    public void create_should_throw_when_sameEmailFound(){
        User mockNewUser = Helper.createMockActiveAdmin();
        Mockito.when(userMockRepository.getUniqueByField("username", mockNewUser.getUsername())).thenThrow(ObjectNotFoundException.class);
        Mockito.when(userMockRepository.getUniqueByField("email", mockNewUser.getEmail())).thenReturn(Helper.createMockActiveRegUser());

        Assertions.assertThrows(DuplicateEmailException.class, () -> userService.create(mockNewUser));
    }

    @Test
    public void isEmailUnique_should_returnFalse_when_sameEmailFound() {
        String testEmail = "test@email.com";
        int testUserId = 4;
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(testEmail, testUserId)).thenReturn(Helper.createMockActiveRegUser());

        //Act
        boolean result = userService.isEmailUnique(testEmail, testUserId);

        //Assert
        Assertions.assertEquals(false, result);
    }

    @Test
    public void isEmailUnique_should_returnTrue_when_noSuchEmailFound() {
        String testEmail = "test@email.com";
        int testUserId = 4;
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(testEmail, testUserId)).thenThrow(ObjectNotFoundException.class);

        //Act
        boolean result = userService.isEmailUnique(testEmail, testUserId);

        //Assert
        Assertions.assertEquals(true, result);
    }

    @Test
    public void updateSelf_should_throwException_when_emailIsDuplicate() {
        User mockUser = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(
                        mockUser.getEmail(), mockUser.getUser_id()))
                .thenReturn(Helper.createMockBlockedRegUser());

        //Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.updateSelf(mockUser));
    }

    @Test
    public void updateSelf_should_callRepository_when_emailIsUnique() {
        User mockUser = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(
                        mockUser.getEmail(), mockUser.getUser_id()))
                .thenThrow(ObjectNotFoundException.class);
        //Act
        userService.updateSelf(mockUser);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void updateUserById_should_throwException_when_initiatedByRegularUser() {
        User initiator = Helper.createMockActiveRegUser();
        User userToUpd = Helper.createMockBlockedRegUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.updateById(initiator, userToUpd));
    }

    @Test
    public void updateUserById_should_throwException_when_initiatedByBlockedAdmin() {
        User initiator = Helper.createMockBlockedAdmin();
        User userToUpd = Helper.createMockBlockedRegUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.updateById(initiator, userToUpd));
    }

    @Test
    public void updateUserById_should_throwException_when_initiatedByActiveOrganizer() {
        User initiator = Helper.createMockActiveOrganizer();
        User userToUpd = Helper.createMockBlockedRegUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.updateById(initiator, userToUpd));
    }

    @Test
    public void updateUserById_should_throwException_when_emailIsDuplicate() {
        User initiator = Helper.createMockActiveAdmin();
        User mockUser = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(
                        mockUser.getEmail(), mockUser.getUser_id()))
                .thenReturn(Helper.createMockBlockedRegUser());

        //Act,Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userService.updateById(initiator, mockUser));
    }

    @Test
    public void updateUserById_should_callRepositoryUpdateUserFields_when_validInfo() {
        User initiator = Helper.createMockActiveAdmin();
        User mockUser = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(
                mockUser.getEmail(), mockUser.getUser_id())).thenThrow(ObjectNotFoundException.class);

        userService.updateById(initiator, mockUser);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUser);
    }

    @Test
    public void deleteUser_should_throwException_when_initiatedByRegularUser() {
        User initiator = Helper.createMockActiveRegUser();
        User mockUser = Helper.createMockBlockedRegUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.delete(mockUser.getUser_id(), initiator));
    }

    @Test
    public void deleteUser_should_throwException_when_initiatedByBlockedAdmin() {
        User initiator = Helper.createMockBlockedAdmin();
        User mockUser = Helper.createMockBlockedRegUser();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.delete(mockUser.getUser_id(), initiator));
    }

    @Test
    public void deleteUser_should_throwException_when_idDoesNotExist() {
        User initiator = Helper.createMockActiveAdmin();
        User userWhichDoesNotExist = Helper.createMockActiveRegUser();
        Mockito.when(userMockRepository.getById(userWhichDoesNotExist.getUser_id())).thenThrow(
                ObjectNotFoundException.class);

        //Act,Assert
        Assertions.assertThrows(ObjectNotFoundException.class, () -> userService.delete(userWhichDoesNotExist.getUser_id(), initiator));
    }

    @Test
    public void deleteUser_should_callRepositoryAndMapper_when_idExists(){
        User initiator = Helper.createMockActiveAdmin();
        User userToBeDeleted = Helper.createMockBlockedRegUser();
        User deletedUser = Helper.createMockDeletedRegUser();
        Mockito.when(userMockRepository.getById(userToBeDeleted.getUser_id())).thenReturn(userToBeDeleted);
        Mockito.when(deletedMockUserMapper.userToDeletedUser(userToBeDeleted)).thenReturn(deletedUser);
        Mockito.when(userMockRepository.emailIsEmailOfOtherUser(deletedUser.getEmail(), deletedUser.getUser_id()))
                .thenThrow(ObjectNotFoundException.class);
        Mockito.when(userMockRepository.update(deletedUser)).thenReturn(deletedUser);

        userService.delete(userToBeDeleted.getUser_id(),initiator);

        Mockito.verify(userMockRepository,Mockito.times(2)).getById(userToBeDeleted.getUser_id());
        Mockito.verify(deletedMockUserMapper, Mockito.times(1)).userToDeletedUser(userToBeDeleted);
        Mockito.verify(userMockRepository, Mockito.times(1)).emailIsEmailOfOtherUser(deletedUser.getEmail(), deletedUser.getUser_id());
        Mockito.verify(userMockRepository, Mockito.times(1)).update(deletedUser);
    }

    @Test
    public void getNumberOfUsers_should_callRepository(){
        userService.getNumberOfUsers();

        Mockito.verify(userMockRepository,Mockito.times(1)).getNumberOfUsers();
    }

    @Test
    public void getListWithBadgeAtLeast_should_throw_when_initiatorIsBlockedOrganizer_and_userIdExists(){
        User initiator = Helper.createMockBlockedOrganizer();

        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getActiveWithRoleIdAndBadgeAtLeast(1, 3, initiator));
    }

    @Test
    public void getListWithBadgeAtLeast_should_throwException_when_initiatorIsRegularUser() {
        User user = Helper.createMockActiveRegUser();
        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> userService.getActiveWithRoleIdAndBadgeAtLeast(1, 3, user));
    }

    @Test
    public void getAll_should_callRepository_when_initiatorIsActiveOrganizer() {
        //Arrange
        User initiator = Helper.createMockActiveOrganizer();
        Mockito.when(userMockRepository.getActiveWithRoleIdAndBadgeAtLeast(1, 3)).thenReturn(new ArrayList<>());

        //Act
        userService.getActiveWithRoleIdAndBadgeAtLeast(1, 3, initiator);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).getActiveWithRoleIdAndBadgeAtLeast(1, 3);
    }

    @Test
    public void getByResetPasswordToken_should_callRepository(){
        userService.getByResetPasswordToken("token");

        Mockito.verify(userMockRepository,Mockito.times(1)).findByResetPasswordToken("token");
    }

    @Test
    public void updatePassword_should_updatePassword(){
        User user = Helper.createMockActiveRegUser();
        String password = "newPassword";
        userService.updatePassword(user,password);

        Assertions.assertEquals(user.getPassword(),password);
    }
}
